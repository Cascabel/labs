class Circle {
	private double Radius;
	private double Ference;
	private double Area;
	
	void SetRadius(double r) {
		Radius = r;
		Ference = 2*Math.PI*r;
		Area = r*r*Math.PI;		
	}
	void SetFerence(double l) {
		Ference = l;
		Radius = l/(2*Math.PI);
		Area = l*l/(4*Math.PI);	
	}
	void SetArea(double s) {
		Area = s;
		Radius = Math.pow(s/Math.PI,0.5);
		Ference = 2*Math.PI*Math.pow(s/Math.PI,0.5);	
	}	
	double GetRadius() {
		return Radius;			
	}
	double GetFerence() {
		return Ference;			
	}
	double GetArea() {
		return Area;			
	}	
}
public class Earth_Rope2 {
	public static void main(String[] args) {
		Circle c1 = new Circle();
		c1.SetRadius(6378100);		
		Circle c2 = new Circle();
		c2.SetFerence(c1.GetFerence()+1);
		System.out.format("�����: %.3f �����", (c2.GetRadius()-c1.GetRadius()));
	}
}